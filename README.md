# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| color picker                                     | 1~5%      | Y         |


---

### How to use 

    簡單的點擊下面的工具來選擇要使用的工具，使用下方的滑條和右邊的調色盤來選擇粗細和顏色。

### Function description

    color picker:可以擷取滑鼠點擊地方的顏色，讓你可以擷取到自己上傳圖片的顏色
    line :一個簡單畫直線工具
    rainbow pen:跟pen一樣，但是無法自己選顏色，會隨著移動改變自身顏色

### Gitlab page link

    "https://107062122.gitlab.io/AS_01_WebCanvas"

### Others (Optional)
    感覺這次幾乎所有東西都是沒學過的，邊打邊查到後面的功能都能自己拚出來，這種感覺很有成就感!


<style>
table th{
    width: 100%;
}
</style>