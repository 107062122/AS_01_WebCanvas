window.onload = function() {
    init();
    color_init();
    color2_init();
    window.addEventListener('popstate', re_undo, false);
}
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
var isMousedown = false;

var canvas2 = document.getElementById('myCanvas2');
var ctx2 = canvas2.getContext('2d');
var isMousedown2 = false;

var x1 = 0;
var x2 = 0;
var y1 = 0;
var y2 = 0;

var temp_imagedata;

//change by button
var tooltype = 0;
var Lwidth = 1;
var eraser_radius = 1;
var color_result = "#000000";
//value use for un/redo
var isDrawing = false;

//value use for change input text's CSS
var input_text = document.querySelector('.input_text');
var text_x;
var text_y;

//value use for change cursor icon
var cursor_icon = document.querySelector('.mycanvas');

//value use for upload image
var image_loader = document.getElementById('upload_image');
image_loader.addEventListener('change', uploadimage, false);

//value use for rainbow
var seven = 0;
var R = 255;
var G = 0;
var B = 0;

function change_size() {
    Lwidth = document.getElementById("size").value;
    eraser_radius = Lwidth;
}


function init() {
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);


    canvas.addEventListener('mousemove', (event) => {
        if (tooltype == "pen") {
            if (!isMousedown) return;
            ctx.globalCompositeOperation = "source-over";
            x2 = event.offsetX;
            y2 = event.offsetY;
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.strokeStyle = color_result;
            ctx.stroke();
            x1 = x2;
            y1 = y2;
        } else if (tooltype == "eraser") {
            if (!isMousedown) return;
            ctx.globalCompositeOperation = "destination-out";
            x2 = event.offsetX;
            y2 = event.offsetY;
            ctx.beginPath();
            ctx.arc(x2, y2, eraser_radius, 0, 2 * Math.PI);
            ctx.fill();

            //維持滑鼠移動時的平滑
            var esin = eraser_radius * Math.sin(Math.atan((y2 - y1) / (x2 - x1)));
            var ecos = eraser_radius * Math.cos(Math.atan((y2 - y1) / (x2 - x1)));
            var x3 = x1 + esin;
            var y3 = y1 - ecos;
            var x4 = x1 - esin;
            var y4 = y1 + ecos;
            var x5 = x2 + esin;
            var y5 = y2 - ecos;
            var x6 = x2 - esin;
            var y6 = y2 + ecos;
            ctx.beginPath();
            ctx.moveTo(x3, y3);
            ctx.lineTo(x5, y5);
            ctx.lineTo(x6, y6);
            ctx.lineTo(x4, y4);
            ctx.closePath();
            ctx.fill();

            x1 = x2;
            y1 = y2;
        } else if (tooltype == "rect") {
            if (!isMousedown) return;
            x2 = event.offsetX;
            y2 = event.offsetY;
            reset_canvas();
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x1, y2);
            ctx.lineTo(x2, y2);
            ctx.lineTo(x2, y1);
            ctx.lineTo(x1, y1);
            ctx.stroke();
        } else if (tooltype == "trangle") {
            if (!isMousedown) return;
            x2 = event.offsetX;
            y2 = event.offsetY;
            reset_canvas();
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y1);
            ctx.lineTo(parseInt((x2 - x1) / 2 + x1), y2);
            ctx.lineTo(x1, y1);
            ctx.stroke();
        } else if (tooltype == "circle") {
            if (!isMousedown) return;
            x2 = event.offsetX;
            y2 = event.offsetY;
            reset_canvas();
            ctx.beginPath();
            x2 = parseInt((x2 + x1) / 2);
            y2 = parseInt((y1 + y2) / 2);
            ctx.arc(x2, y2, Math.abs(parseInt(x2 - x1)), 0, 2 * Math.PI);
            ctx.stroke();
        } else if (tooltype == "line") {
            if (!isMousedown) return;
            x2 = event.offsetX;
            y2 = event.offsetY;
            reset_canvas();
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.stroke();
        } else if (tooltype == "rainbow pen") {
            if (!isMousedown) return;
            ctx.globalCompositeOperation = "source-over";
            x2 = event.offsetX;
            y2 = event.offsetY;
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            if (seven == 0) {
                R = 255;
                G = G + 5;
                B = 0;
                if (G >= 255) {
                    seven = 1;
                }
            } else if (seven == 1) {
                R = R - 5;
                G = 255;
                B = 0;
                if (R <= 0) {
                    seven = 2;
                }
            } else if (seven == 2) {
                R = 0;
                G = 255;
                B = B + 5;
                if (B >= 255) {
                    seven = 3;
                }
            } else if (seven == 3) {
                R = 0;
                G = G - 5;
                B = 255;
                if (G <= 0) seven = 4;
            } else if (seven == 4) {
                R = R + 5;
                G = 0;
                B = 255;
                if (R >= 255) seven = 5;
            } else if (seven == 5) {
                R = 255;
                G = 0;
                B = B - 5;
                if (B <= 0) seven = 0;
            } else {
                seven = 0;
            }


            ctx.strokeStyle = 'rgb(' + R + ',' + G + ',' + B + ')';
            ctx.stroke();
            x1 = x2;
            y1 = y2;
        }
    });
    canvas.addEventListener('mousedown', (event) => {
        if (tooltype == "pen") {
            isMousedown = true;
            x1 = event.offsetX;
            y1 = event.offsetY;
            ctx.lineWidth = Lwidth;
            ctx.lineCap = 'round';
            ctx.lineJoin = 'round';

        } else if (tooltype == "eraser") {
            isMousedown = true;
            x1 = event.offsetX;
            y1 = event.offsetY;
            ctx.globalCompositeOperation = "destination-out";
            ctx.beginPath()
            ctx.arc(x1, y1, eraser_radius, 0, 2 * Math.PI);
            ctx.fill();
        } else if (tooltype == "text") {
            ctx.globalCompositeOperation = "source-over";
            input_text.style.display = 'block';
            input_text.style.top = event.offsetY + 'px';
            input_text.style.left = event.offsetX + 'px';
            text_x = event.offsetX;
            text_y = event.offsetY;
        } else if (tooltype == "rect") {
            isMousedown = true;
            ctx.globalCompositeOperation = "source-over";
            temp_imagedata = ctx.getImageData(0, 0, canvas.width, canvas.height);
            ctx2.putImageData(temp_imagedata, 1, 1);
            reset_canvas();
            x1 = event.offsetX;
            y1 = event.offsetY;
            ctx.lineWidth = Lwidth;
            ctx.strokeStyle = color_result;
            ctx.lineJoin = 'miter';
            x2 = x1;
            y2 = y1;
        } else if (tooltype == "trangle") {
            isMousedown = true;
            ctx.globalCompositeOperation = "source-over";
            temp_imagedata = ctx.getImageData(0, 0, canvas.width, canvas.height);
            ctx2.putImageData(temp_imagedata, 1, 1);
            reset_canvas();
            x1 = event.offsetX;
            y1 = event.offsetY;
            ctx.lineWidth = Lwidth;
            ctx.strokeStyle = color_result;
            ctx.lineCap = 'round';
            ctx.lineJoin = 'round';
            x2 = x1;
            y2 = y1;
        } else if (tooltype == "circle") {
            isMousedown = true;
            ctx.globalCompositeOperation = "source-over";
            temp_imagedata = ctx.getImageData(0, 0, canvas.width, canvas.height);
            ctx2.putImageData(temp_imagedata, 1, 1);
            reset_canvas();
            x1 = event.offsetX;
            y1 = event.offsetY;
            ctx.lineWidth = Lwidth;
            ctx.strokeStyle = color_result;
            ctx.lineCap = 'round';
            ctx.lineJoin = 'round';
            x2 = x1;
            y2 = y1;
        } else if (tooltype == 'color picker') {
            getPixel(event.offsetX, event.offsetY, ctx);
        } else if (tooltype == "line") {
            isMousedown = true;
            ctx.globalCompositeOperation = "source-over";
            temp_imagedata = ctx.getImageData(0, 0, canvas.width, canvas.height);
            ctx2.putImageData(temp_imagedata, 1, 1);
            reset_canvas();
            x1 = event.offsetX;
            y1 = event.offsetY;
            ctx.lineWidth = Lwidth;
            ctx.strokeStyle = color_result;
            ctx.lineCap = 'round';
            ctx.lineJoin = 'round';
            x2 = x1;
            y2 = y1;
        } else if (tooltype == "rainbow pen") {
            isMousedown = true;
            x1 = event.offsetX;
            y1 = event.offsetY;
            ctx.lineWidth = Lwidth;
            ctx.lineCap = 'round';
            ctx.lineJoin = 'round';
        }

    });
    canvas.addEventListener('mouseup', (event) => {
        if (isMousedown) {
            if (tooltype == "pen" || tooltype == "eraser" || tooltype == "rainbow pen") {
                var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
            }
            if (tooltype == "rect") {
                ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
                ctx.putImageData(temp_imagedata, 0, 0);
                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(x1, y2);
                ctx.lineTo(x2, y2);
                ctx.lineTo(x2, y1);
                ctx.lineTo(x1, y1);
                ctx.stroke();
                var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
            } else if (tooltype == "trangle") {
                ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
                ctx.putImageData(temp_imagedata, 0, 0);
                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(x2, y1);
                ctx.lineTo(parseInt((x2 - x1) / 2 + x1), y2);
                ctx.lineTo(x1, y1);
                ctx.stroke();
                var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
            } else if (tooltype == "circle") {
                ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
                ctx.putImageData(temp_imagedata, 0, 0);
                ctx.beginPath();
                ctx.arc(x2, y2, Math.abs(parseInt(x2 - x1)), 0, 2 * Math.PI);
                ctx.stroke();
                var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
            } else if (tooltype == "line") {
                ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
                ctx.putImageData(temp_imagedata, 0, 0);
                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(x2, y2);
                ctx.stroke();
                var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
                window.history.pushState(state, null);
            }
        }
        isMousedown = false;
    })
}

function change_tool(type) {
    if (tooltype == "text") {
        input_text.style.display = 'none';
        document.getElementById('text_input').value = '';
    }
    if (type == 'pen') {
        cursor_icon.style.cursor = 'url(img/pen_cur.png),default';
    } else if (type == 'text') {
        cursor_icon.style.cursor = 'text';
    } else if (type == 'eraser') {
        cursor_icon.style.cursor = 'url(img/eraser_cur.png),auto';
    } else if (type == 'rect') {
        cursor_icon.style.cursor = 'url(img/rectangle.png),auto';
    } else if (type == 'trangle') {
        cursor_icon.style.cursor = 'url(img/trangle.png),auto';
    } else if (type == 'circle') {
        cursor_icon.style.cursor = 'url(img/circle.png),auto';
    } else if (type == 'color picker') {
        cursor_icon.style.cursor = 'url(img/dropper_cur.png),auto'
    } else if (type == 'line') {
        cursor_icon.style.cursor = 'url(img/line_cur.png),auto'
    } else if (type == 'rainbow pen') {
        cursor_icon.style.cursor = 'url(img/pen_cur.png),auto'
    }
    tooltype = type;
}

function reset_canvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}
//下載
function download_png() {
    let imgUrl = canvas.toDataURL('image/png');
    let saveAs = document.createElement('a');
    document.body.appendChild(saveAs);
    saveAs.href = imgUrl;
    saveAs.download = 'download' + (new Date).getTime();
    saveAs.target = '_new';
    saveAs.click();
}
//上傳
function uploadimage(e) {
    console.log(1111);
    var reader = new FileReader();
    reader.onload = function(event) {
        var img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

function upload_png(b) {
    b.click();
}


//調色盤 用三個canvas實作
var color = document.getElementById('colorCanvas');
var ctxc = color.getContext('2d');
var cisMousedown = false;

var color2 = document.getElementById('colorCanvas2');
var ctxc2 = color2.getContext('2d');
var cisMousedown2 = false;
var color_x1 = 0;
var color_y1 = 0;
var cresult = document.getElementById('colorResult');
var rctx = cresult.getContext('2d');

var color2_selector = '#FF0000';

//監控第一個canvas的滑鼠點擊，透過這樣改變顏色
function color_init() {

    refresh_grd();
    color2_selector_result();
    color.addEventListener('mousemove', (event) => {
        if (!cisMousedown) return;
        color_x1 = event.offsetX;
        color_y1 = event.offsetY;

        getPixel(color_x1, color_y1, ctxc);
    });
    color.addEventListener('mousedown', (event) => {
        color_x1 = event.offsetX;
        color_y1 = event.offsetY;
        cisMousedown = true;

        getPixel(color_x1, color_y1, ctxc);
    });
    color.addEventListener('mouseup', (event) => {
        cisMousedown = false;
    });
}

function color2_init() {
    var grdent = ctxc2.createLinearGradient(0, 0, 0, 256);
    grdent.addColorStop(0, 'rgb(255,0,0)');
    grdent.addColorStop(0.1667, 'rgb(255,255,0)');
    grdent.addColorStop(0.3333, 'rgb(0,255,0)');
    grdent.addColorStop(0.5, 'rgb(0,255,255)');
    grdent.addColorStop(0.6667, 'rgb(0,0,255)');
    grdent.addColorStop(0.8333, 'rgb(255,0,255)');
    grdent.addColorStop(1, 'rgb(255,0,0)');
    ctxc2.fillStyle = grdent;
    ctxc2.fillRect(0, 0, 50, 256);
    let x1;
    let y1;
    color2.addEventListener('mousemove', (event) => {
        if (!cisMousedown2) return;
        x1 = event.offsetX;
        y1 = event.offsetY;
        getPixel2(x1, y1, ctxc2);
    });
    color2.addEventListener('mousedown', (event) => {
        x1 = event.offsetX;
        y1 = event.offsetY;
        cisMousedown2 = true;
        getPixel2(x1, y1, ctxc2);
    });
    color2.addEventListener('mouseup', (event) => {
        cisMousedown2 = false;
    });

}


function getPixel(x, y, content) {
    let pixel = content.getImageData(x, y, 1, 1);
    let data = pixel.data;
    //console.log(x, y);
    let RGB = 'rgb(' + data[0] + ',' + data[1] + ',' + data[2] + ')';
    color_result = RGB;
    //console.log(RGB);
    color2_selector_result();
}

function getPixel2(x, y, content) {
    if (y > 255) {
        y = 255;
        cisMousedown2 = false;
    } else if (y < 0) {
        y = 0;
        cisMousedown2 = false;
    }

    let pixel = content.getImageData(12, y, 1, 1);
    let data = pixel.data;
    let RGB = 'rgb(' + data[0] + ',' + data[1] + ',' + data[2] + ')';
    color2_selector = RGB;
    refresh_grd();

    //console.log(RGB);
    getPixel(color_x1, color_y1, ctxc);

}
//顯示目前選中的顏色
function color2_selector_result() {
    rctx.beginPath();
    rctx.arc(25, 25, 25, 0, 2 * Math.PI);
    rctx.fillStyle = color_result;
    rctx.fill();
    //console.log('-----------');
}

function refresh_grd() {
    ctxc.clearRect(0, 0, color.width, color.height);
    ctxc.globalCompositeOperation = 'lighter';
    var grd1 = ctxc.createLinearGradient(0, 0, 0, 255);
    grd1.addColorStop(0, "#000000");
    grd1.addColorStop(1, "#FFFFFF");
    ctxc.fillStyle = grd1;
    ctxc.fillRect(0, 0, 255, 255);
    var grd = ctxc.createLinearGradient(255, 0, 0, 0);
    //console.log(color2_selector);
    grd.addColorStop(0, color2_selector);
    grd.addColorStop(1, "#000000");
    ctxc.fillStyle = grd;
    ctxc.fillRect(0, 0, 255, 255);
}


//re/undo
function canvas_undo() {
    window.history.go(-1);
}

function canvas_redo() {
    window.history.go(1);
}

function re_undo(event) {
    reset_canvas();
    if (event.state) {
        ctx.putImageData(event.state, 0, 0);
    }
}
//text input
document.getElementById('text_input').addEventListener('keydown', function(e) {
    //console.log(e.keyCode);
    if (e.keyCode == 13) {
        //console.log(2);
        input_text.style.display = 'none';
        let text_value = document.getElementById('text_input').value;
        //console.log(text_value);
        text_input_fill(text_value);
        document.getElementById('text_input').value = '';
    }
});

function text_input_fill(text_value) {
    let font_id = document.getElementById('font_selector');
    let font_size = document.getElementById('font_size');
    ctx.font = font_size.options[font_size.selectedIndex].value + ' ' + font_id.options[font_id.selectedIndex].value;
    console.log(ctx.font);
    ctx.fillText(text_value, text_x + 12, text_y + 20);
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
}

function clear_canvas() {
    var con = confirm("reset the canvas?");
    if (con) ctx.clearRect(0, 0, canvas.width, canvas.height);
}